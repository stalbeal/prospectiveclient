package co.com.core

import java.io.IOException

class ValueNotFoundForKeyException(key: String) : NullPointerException("Value not found for key: $key")

class BadRequestException(override val message: String? = null) : IOException(message)
class LoginException(override val message: String? = null) : IOException(message)
class UserCredentialsNotFoundException(override val message: String? = null) : NullPointerException(message)
class TokenNotFoundException(override val message: String? = null) : NullPointerException(message)
class NoProspectToEditFoundException(override val message: String? = null): NullPointerException(message)


