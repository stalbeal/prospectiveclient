package co.com.core

const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
const val BASE_URL = "http://directotesting.igapps.co/"
const val DATABASE_NAME = "prospectiveclient.db"
const val PREF_NAME = "co.com.prospectiveclient"
const val TOKEN_KEY = "TOKEN_KEY"
const val USER_CREDENTIALS_KEY = "USER_CREDENTIALS_KEY"
const val ARGUMENT_VIEW_PROSPECT = "ARGUMENT_VIEW_PROSPECT"
const val REQUEST_CODE_EDIT_PROSPECT = 100
const val DEFAUlT_ERROR = "Algo salió mal, por favor intenta luego"
const val ENCRYPT_PASSWORD = "ujhnbg"
const val ENCRYPTION_ALGORITHM = "AES"
const val ENCRYPTION_TRASNFORMATION = "AES/CBC/PKCS5Padding"
const val UTF_8 = "UTF-8"
const val PKCS5_SALT_LENGTH = 32
const val ITERATION_COUNT = 1000
const val KEY_LENGTH = 256
const val PBKDF2_DERIVATION_ALGORITHM = "PBKDF2WithHmacSHA1"
const val DELIMITER = "]"
const val ENCRYPTION_STRING_FORMAT = "%s%s%s%s%s"
