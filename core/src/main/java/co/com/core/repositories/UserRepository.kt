package co.com.core.repositories

import co.com.core.models.UserCredentials

interface UserRepository {

    suspend fun saveUserCredentials(userCredentials: UserCredentials)

    suspend fun getUserCredentials(): UserCredentials

    suspend fun getToken(): String
}