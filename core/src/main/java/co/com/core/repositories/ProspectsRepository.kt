package co.com.core.repositories

import co.com.core.models.Prospect

interface ProspectsRepository {

    suspend fun getProspectsAsync(): List<Prospect>

    suspend fun insertProspects(prospects: List<Prospect>)

    suspend fun insertProspect(prospect: Prospect)

    suspend fun getProspect(id: String) : Prospect

    suspend fun updateProspect(prospect: Prospect)

    suspend fun getEditedProspects() : List<Prospect>?

    suspend fun getProspects() : List<Prospect>?

}
