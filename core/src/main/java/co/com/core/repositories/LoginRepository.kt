package co.com.core.repositories


interface LoginRepository {

    suspend fun loginAsync(email: String, password: String): String

    suspend fun logOut()
}

