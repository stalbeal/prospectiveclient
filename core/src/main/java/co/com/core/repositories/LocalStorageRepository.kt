package co.com.core.repositories

interface LocalStorageRepository  {

    fun save(key: String, value: String)

    fun get(key: String): String

    fun delete(key: String)
}