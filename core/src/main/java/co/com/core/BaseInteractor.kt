package co.com.core

interface Interactor<Params, Response> where Response : Any {

    suspend operator fun invoke(params: Params): Response
}

