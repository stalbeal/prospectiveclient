package co.com.core.models

data class LoginRequest(val email: String, val password: String)
