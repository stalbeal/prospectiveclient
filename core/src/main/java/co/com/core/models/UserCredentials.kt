package co.com.core.models

data class UserCredentials(
    val email: String,
    val password: String
)