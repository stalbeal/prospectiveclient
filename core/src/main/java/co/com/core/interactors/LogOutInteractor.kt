package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.models.None
import co.com.core.repositories.LoginRepository

class LogOutInteractor(private val loginRepository: LoginRepository) : Interactor<None, Unit>{

    override suspend fun invoke(params: None) {
        loginRepository.logOut()
    }
}