package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.models.None
import co.com.core.models.UserCredentials
import co.com.core.repositories.UserRepository

class GetUserCredentialsInteractor(private val userRepository: UserRepository) : Interactor<None, UserCredentials> {

    override suspend fun invoke(params: None): UserCredentials {
        return userRepository.getUserCredentials()
    }
}