package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.models.UserCredentials
import co.com.core.repositories.UserRepository

class SaveUserCredentialsInteractor(private val userRepository: UserRepository) : Interactor<UserCredentials, Unit> {

    override suspend fun invoke(params: UserCredentials) {
        userRepository.saveUserCredentials(params)
    }
}