package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.models.None
import co.com.core.repositories.UserRepository

class GetTokenInteractor(private val userRepository: UserRepository) : Interactor<None, String> {

    override suspend fun invoke(params: None): String {
        return userRepository.getToken()
    }
}