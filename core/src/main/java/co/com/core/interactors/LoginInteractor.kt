package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.models.LoginRequest
import co.com.core.TOKEN_KEY
import co.com.core.repositories.LocalStorageRepository
import co.com.core.repositories.LoginRepository

class LoginInteractor(private val loginRepository: LoginRepository,
                      private val localStorageRepository: LocalStorageRepository) : Interactor<LoginRequest, Unit> {

    override suspend fun invoke(params: LoginRequest) {
        val response = loginRepository.loginAsync(params.email, params.password)
        localStorageRepository.save(TOKEN_KEY, response)
    }

}

