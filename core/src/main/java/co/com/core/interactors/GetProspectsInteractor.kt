package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.LoginException
import co.com.core.models.None
import co.com.core.models.Prospect
import co.com.core.repositories.ProspectsRepository

class GetProspectsInteractor(private val prospectsRepository: ProspectsRepository) : Interactor<None, List<Prospect>> {

    override suspend fun invoke(params: None): List<Prospect> {

        return try {
            val prospectsFromBackend = prospectsRepository.getProspectsAsync()

            val editedProspects = getEditedProspects() ?: listOf()

            val mapProspectIdProspect = HashMap<String, Prospect>()

            editedProspects.map {
                mapProspectIdProspect.put(it.id, it)
            }

            val prospectsToReturn = ArrayList<Prospect>()

            prospectsFromBackend.forEach {
                if (!mapProspectIdProspect.keys.contains(it.id)) {
                    prospectsToReturn.add(it)
                    prospectsRepository.insertProspect(it)
                } else {
                    prospectsToReturn.add(mapProspectIdProspect[it.id]!!)
                    mapProspectIdProspect.remove(it.id)
                }
            }

            mapProspectIdProspect.forEach {
                prospectsToReturn.add(it.value)
            }

            prospectsToReturn

        } catch (t: Throwable) {
            if (t is LoginException) {
                throw t
            }
            getProspectsFromDatabase() ?: throw t
        }
    }

    private suspend fun getProspectsFromDatabase(): List<Prospect>? {
        return prospectsRepository.getProspects()
    }

    private suspend fun getEditedProspects(): List<Prospect>? {
        return prospectsRepository.getEditedProspects()
    }
}