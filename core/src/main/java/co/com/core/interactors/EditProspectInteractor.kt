package co.com.core.interactors

import co.com.core.Interactor
import co.com.core.models.Prospect
import co.com.core.repositories.ProspectsRepository

class EditProspectInteractor(private val prospectsRepository: ProspectsRepository) :
    Interactor<Prospect, Unit> {
    override suspend fun invoke(params: Prospect) {
        return prospectsRepository.updateProspect(params)
    }

}