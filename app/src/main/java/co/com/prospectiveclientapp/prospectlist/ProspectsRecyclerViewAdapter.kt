package co.com.prospectiveclientapp.prospectlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.com.prospectiveclientapp.R
import co.com.prospectiveclientapp.ViewProspect
import java.util.*

class ProspectsRecyclerViewAdapter(private val prospectListCallback: ProspectListCallback) :
    RecyclerView.Adapter<ProspectItemViewHolder>() {

    private val prospects = ArrayList<ViewProspect>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ProspectItemViewHolder {
        return ProspectItemViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_rv_prospect,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(p0: ProspectItemViewHolder, p1: Int) {
        p0.bind(prospects[p1])
        p0.getIvEditAction().setOnClickListener {
            prospectListCallback.onResultSelected(prospects[p0.adapterPosition])
        }
    }

    override fun getItemCount(): Int {
        return prospects.size
    }

    fun add(prospects: List<ViewProspect>) {
        this.prospects.addAll(prospects)
        notifyDataSetChanged()
    }

    fun replace(viewProspect: ViewProspect){
        val itemIndex = prospects.indexOfFirst {
            it.id == viewProspect.id
        }
        prospects.removeAt(itemIndex)
        notifyItemRemoved(itemIndex)
        prospects.add(itemIndex, viewProspect)
        notifyItemInserted(itemIndex)
    }
}