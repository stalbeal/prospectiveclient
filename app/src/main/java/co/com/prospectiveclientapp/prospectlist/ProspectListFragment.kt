package co.com.prospectiveclientapp.prospectlist

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import co.com.core.ARGUMENT_VIEW_PROSPECT
import co.com.core.REQUEST_CODE_EDIT_PROSPECT
import co.com.prospectiveclientapp.BaseApplication
import co.com.prospectiveclientapp.R
import co.com.prospectiveclientapp.ViewProspect
import co.com.prospectiveclientapp.editprospect.EditProspectActivity
import kotlinx.android.synthetic.main.fragment_prospect_list.*
import javax.inject.Inject


class ProspectListFragment : Fragment(), ProspectListContract.View {

    @Inject
    lateinit var presenter: ProspectListContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity?.application as? BaseApplication)?.appComponent?.inject(this)
        presenter.bind(this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_prospect_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    override fun populateViews() {
        context?.let {
            rvProspectList?.layoutManager = LinearLayoutManager(it)
            rvProspectList?.setHasFixedSize(false)
            rvProspectList?.adapter = ProspectsRecyclerViewAdapter(presenter)
        }
    }

    override fun showProspectList(prospects: List<ViewProspect>) {
        (rvProspectList?.adapter as? ProspectsRecyclerViewAdapter)?.add(prospects)
    }

    override fun showErrorAlert(error: String) {
        val context = context ?: return
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progress?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress?.visibility = View.GONE
    }

    override fun launchEditProspectActivity(viewProspect: ViewProspect) {
        val activity = activity ?: return
        startActivityForResult(Intent(activity, EditProspectActivity::class.java).apply {
            putExtra(ARGUMENT_VIEW_PROSPECT, viewProspect)
        }, REQUEST_CODE_EDIT_PROSPECT)
    }

    override fun updateProspectOnList(viewProspect: ViewProspect) {
        (rvProspectList?.adapter as? ProspectsRecyclerViewAdapter)?.replace(viewProspect)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_EDIT_PROSPECT &&
                data != null && data.hasExtra(ARGUMENT_VIEW_PROSPECT)){
            presenter.onProspectUpdated(data.getParcelableExtra(ARGUMENT_VIEW_PROSPECT) as ViewProspect)
        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.unBind()
    }

    companion object {
        @JvmStatic
        fun newInstance() = ProspectListFragment()
    }
}

