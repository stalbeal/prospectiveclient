package co.com.prospectiveclientapp.prospectlist

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import co.com.prospectiveclientapp.R
import co.com.prospectiveclientapp.ViewProspect
import kotlinx.android.synthetic.main.item_rv_prospect.view.*

class ProspectItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(viewProspect: ViewProspect) {
        val stringBuilder = StringBuilder().append(viewProspect.name).append(" ").append(viewProspect.surname)
        view.tvFullName.text = stringBuilder.toString()
        view.tvId.text = viewProspect.id
        view.tvPhone.text = viewProspect.telephone
        view.ivStatusCd.setImageDrawable(view.ivStatusCd.context.resources.getDrawable(setImageStatus(viewProspect.statusCd)))
    }

    private fun setImageStatus(status: Int): Int {
        return when (status) {
            0 -> R.drawable.ic_pending
            1 -> R.drawable.ic_approved
            2 -> R.drawable.ic_accepted
            3 -> R.drawable.ic_rejected
            4 -> R.drawable.ic_disabled
            else ->
                R.drawable.ic_pending
        }
    }

    fun getIvEditAction(): ImageView {
        return view.ivEditAction
    }
}
