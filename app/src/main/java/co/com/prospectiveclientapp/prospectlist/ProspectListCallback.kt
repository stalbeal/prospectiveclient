package co.com.prospectiveclientapp.prospectlist

import co.com.prospectiveclientapp.ViewProspect

interface ProspectListCallback {

    fun onResultSelected(viewProspect: ViewProspect)
}