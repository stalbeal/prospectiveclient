package co.com.prospectiveclientapp.prospectlist

import co.com.core.DEFAUlT_ERROR
import co.com.core.Interactor
import co.com.core.models.None
import co.com.core.models.Prospect
import co.com.prospectiveclientapp.ViewProspect
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

class ProspectListPresenter(private val getProspectsInteractor: Interactor<None, List<Prospect>>) : ProspectListContract.Presenter {

    override var view: ProspectListContract.View? = null
    override val parentJob = Job()

    override fun onViewCreated() {
        view?.populateViews()
        getProspectList()
    }

    private fun getProspectList(){
        view?.showLoading()
        launchJobOnMainDispatchers {
            try {
                val prospectList = withContext(Dispatchers.IO){
                    getProspectsInteractor.invoke(None)
                }
                view?.showProspectList(mapProspectToViewProspect(prospectList))

            }catch (t: Throwable){
                handleException(t)
            }
            view?.hideLoading()
        }
    }

    private fun mapProspectToViewProspect(prospects: List<Prospect>): List<ViewProspect> {
        return prospects.map {
            ViewProspect(
                it.id,
                it.name,
                it.surname,
                it.telephone,
                it.schProspectIdentification,
                it.address,
                it.createdAt,
                it.updatedAt,
                it.statusCd,
                it.zoneCode,
                it.neighborhoodCode,
                it.cityCode,
                it.sectionCode,
                it.roleId,
                it.appointableId,
                it.rejectedObservation,
                it.observation,
                it.disable,
                it.visited,
                it.callcenter,
                it.acceptSearch,
                it.campaignCode,
                it.userId
            )
        }
    }

    override fun onProspectUpdated(viewProspect: ViewProspect) {
        view?.updateProspectOnList(viewProspect)
    }

    override fun handleException(exception: Throwable) {
       exception.printStackTrace()
        view?.showErrorAlert(exception.message ?: DEFAUlT_ERROR)
    }

    override fun onResultSelected(viewProspect: ViewProspect) {
        view?.launchEditProspectActivity(viewProspect)
    }
}