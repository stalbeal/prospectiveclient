package co.com.prospectiveclientapp.prospectlist

import co.com.prospectiveclientapp.BaseCoroutinePresenter
import co.com.prospectiveclientapp.BaseView
import co.com.prospectiveclientapp.ViewProspect

interface ProspectListContract {

    interface View: BaseView {
        fun populateViews()

        fun showErrorAlert(error: String)

        fun showLoading()

        fun hideLoading()

        fun launchEditProspectActivity(viewProspect: ViewProspect)

        fun showProspectList(prospects: List<ViewProspect>)

        fun updateProspectOnList(viewProspect: ViewProspect)

    }

    interface Presenter : BaseCoroutinePresenter<View>, ProspectListCallback {

        fun onViewCreated()

        fun onProspectUpdated(viewProspect: ViewProspect)
    }
}

