package co.com.prospectiveclientapp

import kotlinx.coroutines.*

interface BaseCoroutinePresenter<View: BaseView> {

    var view: View?

    val parentJob: Job

    fun bind(view: View){
        this.view = view
    }

    fun unBind(){
        view = null
        parentJob.apply {
            cancelChildren()
        }
    }

    fun launchJobOnMainDispatchers(job: suspend CoroutineScope.() -> Unit) {
        CoroutineScope(Dispatchers.Main + parentJob).launch {
            job()
        }
    }

    fun handleException(exception: Throwable)
}

