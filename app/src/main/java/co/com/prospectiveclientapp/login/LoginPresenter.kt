package co.com.prospectiveclientapp.login

import co.com.core.*
import co.com.core.models.LoginRequest
import co.com.core.models.None
import co.com.core.models.UserCredentials
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

class LoginPresenter(
    private val loginInteractor: Interactor<LoginRequest, Unit>,
    private val getTokenInteractor: Interactor<None, String>,
    private val saveUserCredentialsInteractor: Interactor<UserCredentials, Unit>,
    private val getUserCredentialsInteractor: Interactor<None, UserCredentials>
) : LoginContract.Presenter {

    override var view: LoginContract.View? = null

    override val parentJob: Job = Job()

    override fun onCreatedView() {
        view?.populateViews()

        view?.showLoading()
        launchJobOnMainDispatchers {
            try {
                withContext(Dispatchers.IO) {
                    getTokenInteractor(None)
                }

                view?.hideLoading()
                view?.launchHomeActivity()
            } catch (t: Throwable) {
                view?.hideLoading()
                getUserCredentials()
            }
        }
    }

    override fun onButtonActionClick(email: String, password: String) {
        makeLogin(email, password)
    }

    private fun getUserCredentials() {
        view?.showLoading()
        launchJobOnMainDispatchers {
            try {
                val credentials = withContext(Dispatchers.IO) {
                    getUserCredentialsInteractor(None)
                }

                view?.setEmail(credentials.email)
                view?.setPassword(credentials.password)

            } catch (t: Throwable) {
                handleException(t)
            }
            view?.hideLoading()
        }
    }

    private fun makeLogin(email: String, password: String) {
        view?.showLoading()
        launchJobOnMainDispatchers {
            try {
                withContext(Dispatchers.IO) {
                    loginInteractor.invoke(LoginRequest(email, password))
                    saveUserCredentialsInteractor(UserCredentials(email, password))
                }

                view?.launchHomeActivity()

            } catch (t: Throwable) {
                handleException(t)
            }

            view?.hideLoading()
        }
    }

    override fun handleException(exception: Throwable) {
        if (exception !is UserCredentialsNotFoundException) {
            view?.showErrorAlert(exception.message ?: DEFAUlT_ERROR)
        }
    }

}

