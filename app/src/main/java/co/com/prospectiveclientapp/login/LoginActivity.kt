package co.com.prospectiveclientapp.login

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import co.com.prospectiveclientapp.BaseApplication
import co.com.prospectiveclientapp.R
import co.com.prospectiveclientapp.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.regex.Pattern
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginContract.View {

    @Inject
    lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        (application as? BaseApplication)?.appComponent?.inject(this)

        presenter.bind(this)
        presenter.onCreatedView()
    }

    override fun populateViews() {
        btnAction?.setOnClickListener {
            val email = etEmail?.text?.toString() ?: return@setOnClickListener

            if(!isAValidEmail(email)){
                showErrorAlert(getString(R.string.email_error))
                return@setOnClickListener
            }

            val password = etPassword?.text?.toString() ?: return@setOnClickListener
            presenter.onButtonActionClick(email, password)
        }
    }

    private fun isAValidEmail(email: String): Boolean{
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    override fun setEmail(email: String) {
        etEmail?.setText(email)
    }

    override fun setPassword(password: String) {
        etPassword?.setText(password)
    }

    override fun showErrorAlert(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun launchHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    override fun showLoading() {
        progress?.visibility = View.VISIBLE
        etEmail?.visibility = View.GONE
        etPassword?.visibility = View.GONE
        btnAction?.visibility = View.GONE
    }

    override fun hideLoading() {
        progress?.visibility = View.GONE
        etEmail?.visibility = View.VISIBLE
        etPassword?.visibility = View.VISIBLE
        btnAction?.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }
}
