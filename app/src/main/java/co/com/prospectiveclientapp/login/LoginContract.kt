package co.com.prospectiveclientapp.login

import co.com.prospectiveclientapp.BaseCoroutinePresenter
import co.com.prospectiveclientapp.BaseView

interface LoginContract {

    interface View : BaseView {
        fun populateViews()

        fun showErrorAlert(message: String)

        fun showLoading()

        fun hideLoading()

        fun setEmail(email: String)

        fun setPassword(password: String)

        fun launchHomeActivity()
    }

    interface Presenter : BaseCoroutinePresenter<View> {
        fun onCreatedView()

        fun onButtonActionClick(email: String, password: String)

    }

}