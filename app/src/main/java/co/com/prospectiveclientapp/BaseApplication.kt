package co.com.prospectiveclientapp

import android.app.Application
import co.com.data.di.DatabaseModule
import co.com.data.di.RepositoryModule
import co.com.data.di.ServiceModule
import co.com.prospectiveclientapp.di.*

class BaseApplication : Application() {

    var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .interactorModule(InteractorModule())
            .presenterModule(PresenterModule())
            .repositoryModule(RepositoryModule())
            .serviceModule(ServiceModule())
            .databaseModule(DatabaseModule())
            .build()
    }


}