package co.com.prospectiveclientapp.home

import co.com.prospectiveclientapp.BaseCoroutinePresenter
import co.com.prospectiveclientapp.BaseView
import com.google.android.material.navigation.NavigationView

interface HomeContract {

    interface View: BaseView {

        fun closeDrawer()

        fun showProspectListFragment()

        fun onLogOut()

        fun showErrorAlert(error:String)

    }

    interface Presenter : BaseCoroutinePresenter<View>,  NavigationView.OnNavigationItemSelectedListener

}