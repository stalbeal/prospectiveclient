package co.com.prospectiveclientapp.home

import android.view.MenuItem
import co.com.core.DEFAUlT_ERROR
import co.com.core.Interactor
import co.com.core.models.None
import co.com.prospectiveclientapp.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

class HomePresenter(private val logOutInteractor: Interactor<None, Unit>) : HomeContract.Presenter {

    override var view: HomeContract.View? = null

    override val parentJob = Job()

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.nav_list -> view?.showProspectListFragment()
            else -> logOut()
        }

        view?.closeDrawer()

        return true
    }

    private fun logOut() {
        launchJobOnMainDispatchers {
            try {
                withContext(Dispatchers.IO) {
                    logOutInteractor(None)
                }

                view?.onLogOut()
            } catch (t: Throwable) {
                handleException(t)
            }

        }
    }

    override fun handleException(exception: Throwable) {

        exception.printStackTrace()
        view?.showErrorAlert(exception.message ?: DEFAUlT_ERROR)
    }

}

