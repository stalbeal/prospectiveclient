package co.com.prospectiveclientapp.home

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import co.com.prospectiveclientapp.BaseApplication
import co.com.prospectiveclientapp.R
import co.com.prospectiveclientapp.login.LoginActivity
import co.com.prospectiveclientapp.prospectlist.ProspectListFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import javax.inject.Inject

class HomeActivity : HomeContract.View, AppCompatActivity() {

    @Inject
    lateinit var presenter: HomeContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        (application as? BaseApplication)?.appComponent?.inject(this)
        presenter.bind(this)
        populateNavigationDrawer()

    }

    private fun populateNavigationDrawer() {
        val toolbar = toolbar ?: return
        val drawerLayout = drawerLayout ?: return
        val navView = navView ?: return

        toolbar.title = getString(R.string.prospects_label)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(presenter)
        navView.setCheckedItem(R.id.nav_list)
        presenter.onNavigationItemSelected(navView.menu.getItem(0))
    }

    override fun onBackPressed() {
        if (drawerLayout?.isDrawerOpen(GravityCompat.START) == true) {
            drawerLayout?.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun closeDrawer() {
        drawerLayout?.closeDrawer(GravityCompat.START)
    }

    override fun showProspectListFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flMain, ProspectListFragment.newInstance(), ProspectListFragment::class.java.name)
            .commitAllowingStateLoss()
    }

    override fun onLogOut() {
        startActivity(Intent(this, LoginActivity::class.java))
        finishAffinity()
    }

    override fun showErrorAlert(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }
}
