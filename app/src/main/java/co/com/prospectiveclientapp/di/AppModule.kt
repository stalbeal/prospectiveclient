package co.com.prospectiveclientapp.di

import android.content.Context
import android.content.SharedPreferences
import co.com.core.BASE_URL
import co.com.core.PREF_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context){

    @Provides
    @Singleton
    fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideSharePreference(): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideBaseURl(): String{
        return  BASE_URL
    }
}