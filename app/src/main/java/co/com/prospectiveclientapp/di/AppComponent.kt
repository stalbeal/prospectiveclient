package co.com.prospectiveclientapp.di

import co.com.prospectiveclientapp.editprospect.EditProspectActivity
import co.com.prospectiveclientapp.home.HomeActivity
import co.com.prospectiveclientapp.login.LoginActivity
import co.com.prospectiveclientapp.prospectlist.ProspectListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, PresenterModule::class])
interface AppComponent {

    fun inject(loginActivity: LoginActivity)

    fun inject(homeActivity: HomeActivity)

    fun inject(prospectListFragment: ProspectListFragment)

    fun inject(editProspectActivity: EditProspectActivity)
}