package co.com.prospectiveclientapp.di

import co.com.core.*
import co.com.core.models.LoginRequest
import co.com.core.models.None
import co.com.core.models.Prospect
import co.com.core.models.UserCredentials
import co.com.prospectiveclientapp.editprospect.EditProspectContract
import co.com.prospectiveclientapp.editprospect.EditProspectPresenter
import co.com.prospectiveclientapp.home.HomeContract
import co.com.prospectiveclientapp.home.HomePresenter
import co.com.prospectiveclientapp.login.LoginContract
import co.com.prospectiveclientapp.login.LoginPresenter
import co.com.prospectiveclientapp.prospectlist.ProspectListContract
import co.com.prospectiveclientapp.prospectlist.ProspectListPresenter
import dagger.Module
import dagger.Provides

@Module(includes = [InteractorModule::class])
class PresenterModule {

    @Provides
    fun provideLoginPresenter(
        loginInteractor: Interactor<LoginRequest, Unit>,
        getTokenInteractor: Interactor<None, String>,
        saveUserCredentialsInteractor: Interactor<UserCredentials, Unit>,
        getUserCredentialsInteractor: Interactor<None, UserCredentials>
    ): LoginContract.Presenter {
        return LoginPresenter(loginInteractor, getTokenInteractor,saveUserCredentialsInteractor, getUserCredentialsInteractor)
    }

    @Provides
    fun provideHomePresenter(logOutInteractor: Interactor<None, Unit>): HomeContract.Presenter {
        return HomePresenter(logOutInteractor)
    }

    @Provides
    fun provideProspectListPresenter(getProspectsInteractor: Interactor<None, List<Prospect>>): ProspectListContract.Presenter {
        return ProspectListPresenter(getProspectsInteractor)
    }

    @Provides
    fun provideEditProspectPresenter(editProspectInteractor: Interactor<Prospect, Unit>): EditProspectContract.Presenter {
        return EditProspectPresenter(editProspectInteractor)
    }
}