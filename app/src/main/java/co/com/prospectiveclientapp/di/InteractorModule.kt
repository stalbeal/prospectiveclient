package co.com.prospectiveclientapp.di

import co.com.core.*
import co.com.core.interactors.*
import co.com.core.models.LoginRequest
import co.com.core.models.None
import co.com.core.models.Prospect
import co.com.core.models.UserCredentials
import co.com.core.repositories.LocalStorageRepository
import co.com.core.repositories.LoginRepository
import co.com.core.repositories.ProspectsRepository
import co.com.core.repositories.UserRepository
import co.com.data.di.RepositoryModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RepositoryModule::class])
class InteractorModule{

    @Provides
    @Singleton
    fun provideLoginInteractor(loginRepository: LoginRepository,
                               localStorageRepository: LocalStorageRepository): Interactor<LoginRequest, Unit>{
        return LoginInteractor(loginRepository, localStorageRepository)
    }

    @Provides
    @Singleton
    fun provideGetProspectInteractor(prospectsRepository: ProspectsRepository):  Interactor<None, List<Prospect>>{
        return GetProspectsInteractor(prospectsRepository)
    }

    @Provides
    @Singleton
    fun provideGetTokenRepository(userRepository: UserRepository): Interactor<None, String>{
        return GetTokenInteractor(userRepository)
    }

    @Provides
    @Singleton
    fun provideGetUserCredentialsInteractor(userRepository: UserRepository): Interactor<None, UserCredentials>{
        return GetUserCredentialsInteractor(userRepository)
    }

    @Provides
    @Singleton
    fun provideSaveUserCredentialsInteractor(userRepository: UserRepository): Interactor<UserCredentials,Unit>{
        return SaveUserCredentialsInteractor(userRepository)
    }

    @Provides
    @Singleton
    fun provideEditProspectInteractor(prospectsRepository: ProspectsRepository): Interactor<Prospect,Unit>{
        return EditProspectInteractor(prospectsRepository)
    }

    @Provides
    @Singleton
    fun provideLogOutInteractor(loginRepository: LoginRepository): Interactor<None, Unit>{
        return LogOutInteractor(loginRepository)
    }
}