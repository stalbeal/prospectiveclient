package co.com.prospectiveclientapp.editprospect

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import co.com.core.ARGUMENT_VIEW_PROSPECT
import co.com.core.NoProspectToEditFoundException
import co.com.core.models.Prospect
import co.com.prospectiveclientapp.BaseApplication
import co.com.prospectiveclientapp.R
import co.com.prospectiveclientapp.ViewProspect
import kotlinx.android.synthetic.main.activity_edit_prospect.*
import javax.inject.Inject

class EditProspectActivity : AppCompatActivity(), EditProspectContract.View {

    @Inject
    lateinit var presenter: EditProspectContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_prospect)

        (application as? BaseApplication)?.appComponent?.inject(this)

        presenter.bind(this)
        val prospect =
            intent?.getParcelableExtra<ViewProspect>(ARGUMENT_VIEW_PROSPECT) ?: throw NoProspectToEditFoundException()
        presenter.onCreatedView(prospect)
    }

    override fun populateViews() {
        supportActionBar?.title = getString(R.string.edit_label)

        btnAction?.setOnClickListener {

            if(!areFieldsValid()){
                return@setOnClickListener
            }

            val etName = etName ?: return@setOnClickListener
            val etSurname = etAddress ?: return@setOnClickListener
            val etTelephone = etTelephone ?: return@setOnClickListener
            val etSchProspectIdentification = etSchProspectIdentification ?: return@setOnClickListener
            val etStatusCd = etStatusCd ?: return@setOnClickListener
            val etZoneCode = etZoneCode ?: return@setOnClickListener
            val etNeighborhoodCode = etNeighborhoodCode ?: return@setOnClickListener
            val etCityCode = etCityCode ?: return@setOnClickListener
            val etSectionCode = etSectionCode ?: return@setOnClickListener
            val etRoleId = etRoleId ?: return@setOnClickListener
            val etRejectedObservation = etRejectedObservation ?: return@setOnClickListener
            val etObservation = etObservation ?: return@setOnClickListener
            val sAcceptSearch = sAcceptSearch ?: return@setOnClickListener
            val etCampaignCode = etCampaignCode ?: return@setOnClickListener

            val prospectToEdit = presenter.getProspectToEdit()

            if (prospectToEdit != null) {
                presenter.onButtonActionPressed(
                    Prospect(
                        prospectToEdit.id,
                        etName.text.toString(),
                        etSurname.text.toString(),
                        etTelephone.text.toString(),
                        etSchProspectIdentification.text.toString(),
                        etAddress.text.toString(),
                        prospectToEdit.createdAt,
                        prospectToEdit.updatedAt,
                        Integer.parseInt(etStatusCd.text.toString()),
                        etZoneCode.text.toString(),
                        etNeighborhoodCode.text.toString(),
                        etCityCode.text.toString(),
                        etSectionCode.text.toString(),
                        Integer.parseInt(etRoleId.text.toString()),
                        prospectToEdit.appointableId,
                        etRejectedObservation.text.toString(),
                        etObservation.text.toString(),
                        prospectToEdit.disable,
                        prospectToEdit.visited,
                        prospectToEdit.callcenter,
                        sAcceptSearch.isChecked,
                        etCampaignCode.text.toString(),
                        prospectToEdit.userId
                    )
                )
            }
        }
    }

    private fun areFieldsValid(): Boolean {

        val parentView = linearLayout ?: return false

        for (i in 0 until  parentView.childCount) {
            val field = linearLayout?.get(i) as? EditText ?: continue
            field.error = null
        }

        for (i in 0 until  parentView.childCount) {
            val field = linearLayout?.get(i) as? EditText ?: continue
            if (!isFieldValueValid(field.text.toString())){
                field.error = getString(R.string.required_field_error)
                return false
            }
        }

        return true
    }

    private fun isFieldValueValid(text: String): Boolean{
        return text.isNotEmpty()
    }

    override fun onProspectUpdated(viewProspect: ViewProspect) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(ARGUMENT_VIEW_PROSPECT, viewProspect)
        })
        finish()
    }

    override fun showErrorAlert(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progress?.visibility = View.VISIBLE
        linearLayout?.visibility = View.GONE
        btnAction?.visibility = View.GONE
    }

    override fun hideLoading() {
        progress?.visibility = View.GONE
        linearLayout?.visibility = View.VISIBLE
        btnAction?.visibility = View.VISIBLE
    }

    override fun setName(text: String) {
        etName?.setText(text)
    }

    override fun setSurname(text: String) {
        etSurname?.setText(text)
    }

    override fun setTelephone(text: String) {
        etTelephone?.setText(text)
    }

    override fun setSchProspectIdentification(text: String) {
        etSchProspectIdentification?.setText(text)
    }

    override fun setAddress(text: String) {
        etAddress?.setText(text)
    }

    override fun setStatusCd(text: String) {
        etStatusCd?.setText(text)
    }

    override fun setZoneCode(text: String) {
        etZoneCode?.setText(text)
    }

    override fun setNeighborhoodCode(text: String) {
        etNeighborhoodCode?.setText(text)
    }

    override fun setCityCode(text: String) {
        etCityCode?.setText(text)
    }

    override fun setSectionCode(text: String) {
        etSectionCode?.setText(text)
    }

    override fun setRoleId(text: String) {
        etRoleId?.setText(text)
    }

    override fun setRejectedObservation(text: String) {
        etRejectedObservation?.setText(text)
    }

    override fun setObservation(text: String) {
        etObservation?.setText(text)
    }

    override fun setAcceptSearch(option: Boolean) {
        sAcceptSearch?.isChecked = option
    }

    override fun setCampaignCode(text: String) {
        etCampaignCode?.setText(text)
    }

    override fun setId(text: String) {
        etId?.setText(text)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }

}

