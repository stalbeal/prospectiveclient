package co.com.prospectiveclientapp.editprospect

import co.com.core.DEFAUlT_ERROR
import co.com.core.Interactor
import co.com.core.models.Prospect
import co.com.prospectiveclientapp.ViewProspect
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

class EditProspectPresenter(private val editProspectInteractor: Interactor<Prospect, Unit>) :
    EditProspectContract.Presenter {
    override var view: EditProspectContract.View? = null

    override val parentJob = Job()

    private var prospectToEdit: ViewProspect? = null

    override fun onButtonActionPressed(prospect: Prospect) {
        view?.showLoading()
        launchJobOnMainDispatchers {
            try {
                withContext(Dispatchers.IO) {
                    editProspectInteractor.invoke(prospect)
                }

                view?.onProspectUpdated(
                    ViewProspect(
                        prospect.id,
                        prospect.name,
                        prospect.surname,
                        prospect.telephone,
                        prospect.schProspectIdentification,
                        prospect.address,
                        prospect.createdAt,
                        prospect.updatedAt,
                        prospect.statusCd,
                        prospect.zoneCode,
                        prospect.neighborhoodCode,
                        prospect.cityCode,
                        prospect.sectionCode,
                        prospect.roleId,
                        prospect.appointableId,
                        prospect.rejectedObservation,
                        prospect.observation,
                        prospect.disable,
                        prospect.visited,
                        prospect.callcenter,
                        prospect.acceptSearch,
                        prospect.campaignCode,
                        prospect.userId
                    )
                )
            } catch (t: Throwable) {
                handleException(t)
            }
            view?.hideLoading()
        }
    }

    override fun onCreatedView(prospect: ViewProspect) {
        prospectToEdit = prospect

        view?.apply {
            populateViews()

            setId(prospect.id)
            setAcceptSearch(prospect.acceptSearch)
            setName(prospect.name)
            setSurname(prospect.surname)
            setTelephone(prospect.telephone)
            setSchProspectIdentification(prospect.schProspectIdentification)
            setAddress(prospect.address)
            setStatusCd(prospect.statusCd.toString())
            setZoneCode(prospect.zoneCode)
            setNeighborhoodCode(prospect.neighborhoodCode)
            setCityCode(prospect.cityCode)
            setSectionCode(prospect.sectionCode)
            setRoleId(prospect.roleId.toString())
            prospect.rejectedObservation?.let { setRejectedObservation(it) }
            setObservation(prospect.observation)
            setAcceptSearch(prospect.acceptSearch)
            setCampaignCode(prospect.campaignCode)
        }
    }

    override fun handleException(exception: Throwable) {
        exception.printStackTrace()
        view?.showErrorAlert(exception.message ?: DEFAUlT_ERROR)
    }

    override fun getProspectToEdit(): ViewProspect? {
        return prospectToEdit
    }
}