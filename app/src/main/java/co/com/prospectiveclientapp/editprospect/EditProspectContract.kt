package co.com.prospectiveclientapp.editprospect

import co.com.core.models.Prospect
import co.com.prospectiveclientapp.BaseCoroutinePresenter
import co.com.prospectiveclientapp.BaseView
import co.com.prospectiveclientapp.ViewProspect

interface EditProspectContract {

    interface View : BaseView {
        fun populateViews()

        fun showErrorAlert(error:String)

        fun showLoading()

        fun hideLoading()

        fun setName(text: String)

        fun setSurname(text: String)

        fun setTelephone(text: String)

        fun setSchProspectIdentification(text: String)

        fun setAddress(text: String)

        fun setStatusCd(text: String)

        fun setZoneCode(text: String)

        fun setNeighborhoodCode(text: String)

        fun setCityCode(text: String)

        fun setSectionCode(text: String)

        fun setRoleId(text: String)

        fun setRejectedObservation(text: String)

        fun setObservation(text: String)

        fun setAcceptSearch(option: Boolean)

        fun setCampaignCode(text: String)

        fun setId(text: String)

        fun onProspectUpdated(viewProspect: ViewProspect)
    }

    interface Presenter : BaseCoroutinePresenter<View>{

        fun onCreatedView(prospect: ViewProspect)

        fun onButtonActionPressed(prospect: Prospect)

        fun getProspectToEdit(): ViewProspect?

    }

}

