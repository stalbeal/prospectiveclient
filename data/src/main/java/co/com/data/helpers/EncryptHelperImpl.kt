package co.com.data.helpers

import android.util.Base64
import co.com.core.*
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

class EncryptHelperImpl : EncryptHelper {

    private val random = SecureRandom()

    override fun decrypt(encryptedText: String): String {
        val fields = encryptedText.split(DELIMITER)
        val salt = fromBase64(fields[0])
        val iv = fromBase64(fields[1])
        val cipherBytes = fromBase64(fields[2])
        val key = generateSecretKey(salt)

        val cipher = Cipher.getInstance(ENCRYPTION_TRASNFORMATION)
        val ivParams = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, key, ivParams)
        val plaintext = cipher.doFinal(cipherBytes)
        return String(plaintext, charset(UTF_8))

    }

    override fun encrypt(textToEncrypt: String): String {
        val key = generateSecretKey(salt)
        val cipher = Cipher.getInstance(ENCRYPTION_TRASNFORMATION)
        val iv = generateIv(cipher.blockSize)
        val ivParams = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, key, ivParams)
        val cipherText = cipher.doFinal(textToEncrypt.toByteArray(charset(UTF_8)))

        return String.format(
            ENCRYPTION_STRING_FORMAT,
            toBase64(salt),
            DELIMITER,
            toBase64(iv),
            DELIMITER,
            toBase64(cipherText)
        )
    }

    private fun generateSecretKey(salt: ByteArray): SecretKey {
        val keySpec = PBEKeySpec(ENCRYPT_PASSWORD.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH)
        val keyFactory = SecretKeyFactory.getInstance(PBKDF2_DERIVATION_ALGORITHM)
        val keyBytes = keyFactory.generateSecret(keySpec).encoded
        return SecretKeySpec(keyBytes, ENCRYPTION_ALGORITHM)
    }

    private fun generateIv(length: Int): ByteArray {
        val b = ByteArray(length)
        random.nextBytes(b)
        return b
    }

    private val salt by lazy {
        val b = ByteArray(PKCS5_SALT_LENGTH)
        random.nextBytes(b)
        b
    }

    private fun toBase64(bytes: ByteArray): String {
        return Base64.encodeToString(bytes, Base64.NO_WRAP)
    }

    private fun fromBase64(base64: String): ByteArray {
        return Base64.decode(base64, Base64.NO_WRAP)
    }
}