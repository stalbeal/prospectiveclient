package co.com.data.helpers

interface EncryptHelper{

    fun encrypt(textToEncrypt: String): String

    fun decrypt(encryptedText: String): String
}