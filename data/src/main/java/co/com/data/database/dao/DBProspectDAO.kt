package co.com.data.database.dao

import androidx.room.*
import co.com.data.database.entities.DBProspect

@Dao
interface DBProspectDAO {

    @Query("select * from prospect where id= :id")
    suspend fun getProspect(id: String): DBProspect

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertProspect(dbProspect: DBProspect)

    @Delete
    suspend fun deleteProspect(dbProspect: DBProspect)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateProspect(dbProspect: DBProspect)

    @Query("select * from prospect")
    suspend fun getProspects(): List<DBProspect>?

    @Query("select * from prospect where edited= 1")
    suspend fun getEditedProspects() : List<DBProspect>?
}