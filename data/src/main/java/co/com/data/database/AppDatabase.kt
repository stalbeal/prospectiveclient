package co.com.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import co.com.data.database.dao.DBProspectDAO
import co.com.data.database.entities.DBProspect

@Database(entities = [(DBProspect::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun apiProspectDAO(): DBProspectDAO

}