package co.com.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "prospect")
data class DBProspect(
    @PrimaryKey val id: String,
    val name: String,
    val surname: String,
    val telephone: String,
    val schProspectIdentification: String,
    val address: String,
    val createdAt: String,
    val updatedAt: String,
    val statusCd: Int,
    val zoneCode: String,
    val neighborhoodCode: String,
    val cityCode: String,
    val sectionCode: String,
    val roleId: Int,
    val appointableId: String? = null,
    val rejectedObservation: String? = null,
    val observation: String,
    val disable: Boolean,
    val visited: Boolean,
    val callcenter: Boolean,
    val acceptSearch: Boolean,
    val campaignCode: String,
    val userId: Int?,
    val edited: Boolean = false
)
