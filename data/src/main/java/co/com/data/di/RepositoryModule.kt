package co.com.data.di

import android.content.SharedPreferences
import co.com.core.repositories.LocalStorageRepository
import co.com.core.repositories.LoginRepository
import co.com.core.repositories.ProspectsRepository
import co.com.core.repositories.UserRepository
import co.com.data.database.dao.DBProspectDAO
import co.com.data.helpers.EncryptHelperImpl
import co.com.data.network.services.LoginRoute
import co.com.data.network.services.ProspectsRoute
import co.com.data.repositories.LocalStorageRepositoryImpl
import co.com.data.repositories.LoginRepositoryImpl
import co.com.data.repositories.ProspectsRepositoryImpl
import co.com.data.repositories.UserRepositoryImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [ServiceModule::class, DatabaseModule::class])
class RepositoryModule {

    @Provides
    @Singleton
    fun provideProspectRepository(
        prospectsRoute: ProspectsRoute,
        apiDBProspectDAO: DBProspectDAO
    ): ProspectsRepository {
        return ProspectsRepositoryImpl(prospectsRoute, apiDBProspectDAO)
    }

    @Provides
    @Singleton
    fun provideLocalStorageRepository(sharedPreferences: SharedPreferences): LocalStorageRepository {
        return LocalStorageRepositoryImpl(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideLoginRepository(
        loginRoute: LoginRoute,
        localStorageRepository: LocalStorageRepository
    ): LoginRepository {
        return LoginRepositoryImpl(loginRoute, localStorageRepository)
    }

    @Provides
    @Singleton
    fun provideUserRepository(
        localStorageRepository: LocalStorageRepository,
        gson: Gson
    ): UserRepository {
        return UserRepositoryImpl(localStorageRepository, gson, EncryptHelperImpl())
    }
}