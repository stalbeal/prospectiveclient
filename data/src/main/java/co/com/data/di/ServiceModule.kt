package co.com.data.di

import android.content.SharedPreferences
import co.com.core.BadRequestException
import co.com.core.LoginException
import co.com.core.TOKEN_KEY
import co.com.data.network.models.APIResponseError
import co.com.data.network.services.LoginRoute
import co.com.data.network.services.ProspectsRoute
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Este modulo provee la instancia de retrofit para ser utilizada en la app
 */
@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideGson(): Gson =
        GsonBuilder()
            //.setDateFormat(Settings.System.DATE_FORMAT)
            .setLenient()
            .create()

    @Provides
    @Singleton
    fun provideLoginInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    @Provides
    @Singleton
    fun provideHttpClient(
        gson: Gson,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        sharedPreferences: SharedPreferences
    ): OkHttpClient {

        return OkHttpClient.Builder().apply {
            connectTimeout(30, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            writeTimeout(30, TimeUnit.SECONDS)
            networkInterceptors().add(httpLoggingInterceptor)
            addTokenInterceptor(sharedPreferences)
            addResponseInterceptor(gson)
        }.build()
    }

    private fun OkHttpClient.Builder.addTokenInterceptor(sharedPreferences: SharedPreferences) {
        addInterceptor { chain: Interceptor.Chain ->
            val request = chain.request()
            val headers = request.headers().values("@")
            val requestBuilder = request.newBuilder()
            if (headers.isNotEmpty()) {
                val token = sharedPreferences.getString(TOKEN_KEY, null)
                requestBuilder.addHeader("token", "$token")
                    .removeHeader("@")
            }

            chain.proceed(requestBuilder.build())
        }
    }

    private fun OkHttpClient.Builder.addResponseInterceptor(gson: Gson) {
        addInterceptor { chain: Interceptor.Chain ->
            val response = chain.proceed(chain.request())

            if (!response.isSuccessful) {
                val errorMessage = getErrorMessage(response, gson)
                throw  when (response.code()) {
                    422 -> LoginException(errorMessage)
                    else -> BadRequestException(errorMessage)
                }
            }

            response
        }
    }

    @Provides
    @Singleton
    fun provideRetroFit(
        gson: Gson, okHttpClient: OkHttpClient,
        baseUrl: String
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }


    @Provides
    @Singleton
    fun provideLoginRoute(retrofit: Retrofit): LoginRoute = retrofit.create(LoginRoute::class.java)

    @Provides
    @Singleton
    fun provideProspectRoute(retrofit: Retrofit): ProspectsRoute {
        return retrofit.create(ProspectsRoute::class.java)
    }

    private fun getErrorMessage(response: Response, gson: Gson): String? {
        return try {
            gson.fromJson(response.body()?.string(), APIResponseError::class.java).error
        } catch (t: Throwable) {
            null
        }
    }

}