package co.com.data.di

import android.content.Context
import androidx.room.Room
import co.com.core.DATABASE_NAME
import co.com.data.database.AppDatabase
import co.com.data.database.dao.DBProspectDAO
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context, AppDatabase::class.java,
            DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()

    }

    @Provides
    @Singleton
    fun provideProspectDAO(appDatabase: AppDatabase): DBProspectDAO {
        return appDatabase.apiProspectDAO()
    }
}

