package co.com.data.repositories

import co.com.core.models.Prospect
import co.com.core.repositories.ProspectsRepository
import co.com.data.database.dao.DBProspectDAO
import co.com.data.database.entities.DBProspect
import co.com.data.network.services.ProspectsRoute

class ProspectsRepositoryImpl(
    private val prospectsRoute: ProspectsRoute,
    private val apiDBProspectDAO: DBProspectDAO
) : ProspectsRepository {

    override suspend fun getProspectsAsync(): List<Prospect> {
        return try {
            prospectsRoute.getProspects().map {
                Prospect(
                    it.id,
                    it.name,
                    it.surname,
                    it.telephone,
                    it.schProspectIdentification,
                    it.address,
                    it.createdAt,
                    it.updatedAt,
                    it.statusCd,
                    it.zoneCode,
                    it.neighborhoodCode,
                    it.cityCode,
                    it.sectionCode,
                    it.roleId,
                    it.appointableId,
                    it.rejectedObservation,
                    it.observation,
                    it.disable,
                    it.visited,
                    it.callcenter,
                    it.acceptSearch,
                    it.campaignCode,
                    it.userId
                )
            }
        } catch (e: Exception) {
            throw e
        }
    }

    override suspend fun insertProspects(prospects: List<Prospect>) {
        prospects.forEach {
            apiDBProspectDAO.insertProspect(mapProspectToDBProspect(it))
        }
    }

    override suspend fun insertProspect(prospect: Prospect) {
        apiDBProspectDAO.insertProspect(mapProspectToDBProspect(prospect))
    }

    override suspend fun getProspect(id: String): Prospect {
        return mapDBProspectToProspect(apiDBProspectDAO.getProspect(id))
    }

    override suspend fun updateProspect(prospect: Prospect) {
        apiDBProspectDAO.updateProspect(mapProspectToDBProspect(prospect, true))
    }

    override suspend fun getEditedProspects(): List<Prospect>? {
        return apiDBProspectDAO.getEditedProspects()?.map {
            mapDBProspectToProspect(it)
        }
    }

    override suspend fun getProspects(): List<Prospect>? {
        return apiDBProspectDAO.getProspects()?.map {
            mapDBProspectToProspect(it)
        }
    }

    private fun mapProspectToDBProspect(prospect: Prospect, edited: Boolean = false): DBProspect {
        return DBProspect(
            prospect.id,
            prospect.name,
            prospect.surname,
            prospect.telephone,
            prospect.schProspectIdentification,
            prospect.address,
            prospect.createdAt,
            prospect.updatedAt,
            prospect.statusCd,
            prospect.zoneCode,
            prospect.neighborhoodCode,
            prospect.cityCode,
            prospect.sectionCode,
            prospect.roleId,
            prospect.appointableId,
            prospect.rejectedObservation,
            prospect.observation,
            prospect.disable,
            prospect.visited,
            prospect.callcenter,
            prospect.acceptSearch,
            prospect.campaignCode,
            prospect.userId,
            edited
        )
    }

    private fun mapDBProspectToProspect(dbProspect: DBProspect): Prospect {
        return Prospect(
            dbProspect.id,
            dbProspect.name,
            dbProspect.surname,
            dbProspect.telephone,
            dbProspect.schProspectIdentification,
            dbProspect.address,
            dbProspect.createdAt,
            dbProspect.updatedAt,
            dbProspect.statusCd,
            dbProspect.zoneCode,
            dbProspect.neighborhoodCode,
            dbProspect.cityCode,
            dbProspect.sectionCode,
            dbProspect.roleId,
            dbProspect.appointableId,
            dbProspect.rejectedObservation,
            dbProspect.observation,
            dbProspect.disable,
            dbProspect.visited,
            dbProspect.callcenter,
            dbProspect.acceptSearch,
            dbProspect.campaignCode,
            dbProspect.userId
        )
    }


}
