package co.com.data.repositories

import co.com.core.*
import co.com.core.models.UserCredentials
import co.com.core.repositories.LocalStorageRepository
import co.com.core.repositories.UserRepository
import co.com.data.helpers.EncryptHelper
import com.google.gson.Gson

class UserRepositoryImpl(
    private val localStorageRepository: LocalStorageRepository,
    private val gson: Gson,
    private val encryptHelper: EncryptHelper
) :
    UserRepository {

    override suspend fun saveUserCredentials(userCredentials: UserCredentials) {
        val encryptedCredentials = encryptHelper.encrypt(gson.toJson(userCredentials))
        localStorageRepository.save(USER_CREDENTIALS_KEY, encryptedCredentials)
    }

    override suspend fun getUserCredentials(): UserCredentials {
        return try {
            val encryptedCredentials = localStorageRepository.get(USER_CREDENTIALS_KEY)
            val credentials = encryptHelper.decrypt(encryptedCredentials)
            gson.fromJson(credentials, UserCredentials::class.java)
        } catch (t: Throwable) {
            throw UserCredentialsNotFoundException(t.message)
        }
    }

    override suspend fun getToken(): String {
        return try {
            localStorageRepository.get(TOKEN_KEY)
        } catch (t: Throwable) {
            throw TokenNotFoundException(t.message)
        }
    }
}