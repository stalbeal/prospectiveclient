package co.com.data.repositories

import co.com.core.TOKEN_KEY
import co.com.core.repositories.LocalStorageRepository
import co.com.core.repositories.LoginRepository
import co.com.data.network.services.LoginRoute

class LoginRepositoryImpl(private val loginRoute: LoginRoute,
                          private val localStorageRepository: LocalStorageRepository) : LoginRepository {

    override suspend fun loginAsync(email: String, password: String): String {
        return try {
            loginRoute.login(email, password).authToken
        } catch (e: Exception) {
            throw e
        }
    }

    override suspend fun logOut() {
        localStorageRepository.delete(TOKEN_KEY)
    }
}