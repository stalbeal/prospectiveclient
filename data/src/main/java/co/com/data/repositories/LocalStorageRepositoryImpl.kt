package co.com.data.repositories

import android.content.SharedPreferences
import co.com.core.ValueNotFoundForKeyException
import co.com.core.repositories.LocalStorageRepository

class LocalStorageRepositoryImpl( private val sharedPreference: SharedPreferences) : LocalStorageRepository {

    override fun save(key: String, value: String) {
        sharedPreference.edit().putString(key, value).apply()
    }

    override fun get(key: String): String{
        return sharedPreference.getString(key, null) ?: throw ValueNotFoundForKeyException(key)
    }

    override fun delete(key: String) {
        sharedPreference.edit().remove(key).apply()
    }

}