package co.com.data.network.models

data class APILogin(val authToken: String, val email: String)

