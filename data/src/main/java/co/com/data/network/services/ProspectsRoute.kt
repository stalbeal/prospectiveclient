package co.com.data.network.services

import co.com.data.network.models.APIProspectResponse
import retrofit2.http.GET
import retrofit2.http.Headers

interface ProspectsRoute {

    @GET("sch/prospects.json")
    @Headers("@:token")
    suspend fun getProspects() : List<APIProspectResponse>
}