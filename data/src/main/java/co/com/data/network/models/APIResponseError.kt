package co.com.data.network.models

data class APIResponseError(val code: String, val error: String)