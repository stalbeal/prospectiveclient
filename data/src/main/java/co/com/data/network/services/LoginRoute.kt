package co.com.data.network.services

import co.com.data.network.models.APILogin
import retrofit2.http.GET
import retrofit2.http.Query

interface LoginRoute {

    @GET("application/login")
    suspend fun login(@Query("email") email: String, @Query("password") password: String) : APILogin
}

